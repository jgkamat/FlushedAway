# FlushedAway

An Inform 7 game by team "Broken" for Engl 1102

Flushed Away is a student made project by 3 students at Georgia Tech. It will be written in inform7, a popular text-adventure engine. You can play the compiled files with gargoyle, which is freely available.

This project can be opened by telling inform7 to open the directory ```Flushed Away.inform```. Source can be found under ```Flushed Away.inform/Source/story.ni```

Please download and play the gblorb file in [Gargoyle](http://ccxvii.net/gargoyle/) for best results.

Thanks to Mark Tilford for writing the mapping extension used in Flushed Away!

This project is Licensed under the GPLv3. Inform7's CLI interface is also licensed under the GPL. Feel free to modify and redistribute, provided you follow the terms of the license.
