
set -e

# This script is called by .travis.yml to update our doxygen documentation, which is stored on the gh-pages branch of this repo
# It's based on this blog post: http://philipuren.com/serendipity/index.php?/archives/21-Using-Travis-to-automatically-publish-documentation.html

git config --global user.name "$GIT_NAME"
git config --global user.email "$GIT_EMAIL"

if [ "$(git rev-parse master)" = "$TRAVIS_COMMIT" ]; then
	mkdir -p api_docs
	git clone -b gh-pages git://github.com/jgkamat/FlushedAway api_docs/html

	# Copile inform here
	sudo i7 -r Flushed\ Away.inform
	sudo cp -r Flushed\ Away.materials/Release/* api_docs/html

	cd api_docs/html/
	git add --all
	git commit -m 'Updated Web Player'
	git push -u https://${GH_TOKEN}@github.com/jgkamat/FlushedAway gh-pages
fi
