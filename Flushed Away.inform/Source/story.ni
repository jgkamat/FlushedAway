"Flushed Away" by "Broken"

Include Automap by Mark Tilford. [Thanks!]
Use automap hide paths through closed doors.

[This program was written for ENGL1102 taught by Dr. Kotchian

Flushed Away's rhetorical effect is to demonstrate that internet memes can have meaning and value beyond what is easily apparent.

**************************************************************
Flushed Away is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Flushed Away is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Flushed Away.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************
]

[ Files ]
Figure of coverimg is the file "Cover.jpg".
Figure of spiderman is the file "spiderman.jpg".
Figure of gggreg is the file "gggreg.jpg".
Figure of astheyappear is the file "astheyappear.jpg".
Figure of gaben is the file "gaben.jpg".
Figure of gotnotime is the file "gotnotime.jpg".
Figure of hourofhammer is the file "hourofhammer.jpg".
Figure of problems is the file "problems.jpg".
Figure of wonka is the file "wonka.jpg".
Figure of beer is the file "beer.jpg".
Sound of 9001 is the file "9001.ogg".

[Layout]

A fdoor is a kind of door.
A fdoor is usually locked.
The description of fdoor is "A door".
The printed name of fdoor is "door".
Understand "door" as fdoor.

Instead of opening fdoor:
	say "This door is locked, and cannot be opened manually!".;

The The Cell is a room. "The mitochondria is teh powahause of teh sell.[line break]Too bad there is no mitochondria in this cell.[line break][line break]There are exits to the northeast and northwest".

[Starting info and coverimg]
To decide whether print banner: 
    if the command prompt is "$ ", yes.; 
Rule for printing the banner text when print banner: do nothing.
When play begins:
	say "[banner text]".;
	Display the Figure of coverimg.;
	say "[line break][bold type]Flushed Away[roman type] Copyright (C) 2015  Team Broken.[line break]This program comes with ABSOLUTELY NO WARRANTY; for details see the included license. This is free software, and you are welcome to redistribute it
    under certain conditions; see the included license for details.[line break][line break]
Flushed Away is best experienced in Gargoyle, played directly on a computer. Playing it in the web browser is supported, but will be without images and sound, which are central to the gameplay! (Please turn on your speakers).".;
	say "[paragraph break]----------------------------------------------------------------------------[line break]".;
	say "[paragraph break]You wake up in a dark empty cell room. The wall appears to be made of solid concrete. You try to remember how you got here. You decide the best way to do this is to start by remembering stuff about yourself. You are a 19 year old freshman. Your focus of study is computer science. You are about 6 feet tall, male, and have black hair. In your opinion you are quite handsome, but you don’t have very good luck with the ladies, so your current predicament most likely did not involve a woman. Were you out partying last night and got arrested? Maybe you are in a jail cell. After all, there is a toilet in the room much like a jail cell. Perhaps you got arrested for underage drinking. But no, wait, that can’t be right. You remember going back to your dorm room and getting ready for bed. Plus you have a calculus test in the morning, there is no way you were out partying. Then what else could have happened? You remember waking up in the middle of the night and  walking down the hall to go to the bathroom and then… huh, thats weird, you don’t remember anything after that. The only thing you do know is you have no clue where you are.[line break]".;
	Now the command prompt is "$ ".

The Hall1 is a room. "The wall of the room has a large image of Spider-Man with writing on it.[line break] You see what looks like a toilet oddly placed in the middle of this room.[line break][line break] There appear to be exits to the east and southeast".
The Hall1 is northwest of The Cell.


The TeleporterRoom2 is a room. "You see an object that looks like a toilet in the center of this room. There is also a solitary image of a stoic man in sunglasses on the wall. The man seems to be deep in thought, almost as if he has just stumbled upon a great truth of the universe.
[line break][line break]A door leads southwest."
The TeleporterRoom2 is northeast of Hall2.

The Hall2 is a room. "The walls of this room are plastered with a familiar face and words,[line break] but you cannot quite identify where you know them from.[line break] Aside from the walls, you do not notice anything special about the room.[line break][line break]There are exits to the northeast, southwest, and west."
The Hall2 is northeast of The Cell.

The Connecting Room is a room. "This room is more open than the others. You see an image of an agitated woman on the wall.[line break][line break] There are exits to the north, south, east, and west."
[note: Include a riddle related object and description of it somewhere in the room, relating it to the meme if possible. Maybe something related to time or a clock.]
The Connecting Room is west of Hall2 and east of Hall1.


The BridgeBlock is a room. "You see an image of an old-timey looking man on the walls and another one of the toilet like objects.[line break] There is an exit to the north."
[include riddle on door and possibly a riddle-related object]
A bdoor is a fdoor. bdoor is south of Connecting Room. bdoor is north of BridgeBlock.

The The Hub is a room. "You see a small speaker and some type of gauge or meter on a console in front of you.[line break][line break] There are LOCKED exits to the south, east, west, and an unlocked one northeast."
The hdoor1 is a fdoor. The hdoor1 is south of The Hub. The hdoor1 is north of the Connecting Room.

The TeleporterRoom1 is a room. "There is another one of the strange toilet devices in this room. On the walls of this room are scrawled pictures of a man in a strange suit. [line break][line break] There are exits to the east, northwest, north, and northeast."
The hdoor2 is a fdoor. The hdoor2 is west of The Hub. The hdoor2 is east of TeleporterRoom1.

The A2 is a room.
A2 is northwest of TeleporterRoom1.
instead of going to A2:
	say "Upon walking into the room, your eyes are assaulted by grotesque images of grown men prancing about with rainbow-color ponies.[line break] This alien race must be the owners of this ship. You immediately turn back from where you came.";
	move player to Cell;


The A3 is a room. "The room is filled with a pleasant atmosphere and appears to be some type of religious shrine.[line break] The words 'Renounce peasantry' and 'GabeN is love, GabeN is life' are written on the walls, and there are multiple images on display as well.[line break] There is an exit to the south.".
A3 is north of TeleporterRoom1.

The A4 is a room. "Ay?[line break][line break]There is an exit north and west.".
The hdoor3 is a fdoor. The hdoor3 is east of The Hub. The hdoor3 is west of A4.
instead of going to A4:
	if (hdoor3 is closed) AND (player is not in A5):
		try going west; [VERY BAAAD HACK. ALSO INFORM CAN STACKOVERFLOW]
	otherwise:
		say "Upon walking into the room, your eyes are assaulted by grotesque images of grown men prancing about with rainbow-color ponies.[line break] This alien race must be the owners of this ship. You immediately turn back from where you came.";
		move player to Cell;

The A5 is a room. "You are in a room with a strange toilet-like device in the center.[line break] The walls are bare. You see exits to the southwest and to the south.".
A5 is northeast of The Hub and north of A4.

The B1 is a room. "On the wall is an image of a man you can't help but look at, everything about him draws you in.[line break][line break]There are exits to the southwest and the north."
.
tdoor1 is a fdoor. tdoor1 is northeast of TeleporterRoom1. tdoor1 is southwest of B1..

The TeleporterRoom3 is a room. "There is another strange toilet device here.[line break] There are exits to the north and east.".
bdoor1 is a fdoor. bdoor1 is north of B1. bdoor1 is south of TeleporterRoom3.

The B2 is a room. "You are in a long hallway with bare walls. You see exits to the north and west.".
B2 is north of TeleporterRoom3.

The TeleporterRoom4 is a room. "You are in a high-tech looking room with two of the toilet-like devices, one on the east side and one on the west side.[line break]You see a few more interesting figures on the walls[line break]There is an exit to the east.".
TeleporterRoom4 is west of B2.

The B3 is a room. "This room contains another strange toilet device. There are exits to the south and east.".
B3 is north of B2.  

The B4 is a room. "You are in what looks like a kitchen. There are exits to the west and north.".
gdoor1 is a fdoor. gdoor1 is east of TeleporterRoom3. gdoor1 is west of B4.

The B5 is a room. "This room contains a bunch of piping and engine components. There are all sorts of strange control mechanisms scattered about the room.[line break]There are exits to the north and south.".
gdoor2 is a fdoor. gdoor2 is north of B4. gdoor2 is south of B5.

The Exit is a room. "There is another strange toilet device in this room.[line break] There are exits to the west and south.".
edoor1 is a fdoor. edoor1 is north of B5. edoor1 is south of exit.
edoor2 is a fdoor. edoor2 is east of B3. edoor2 is west of exit.

[ ROOM NAMES ]
The printed name of Hall1 is "West Cell Entrance".
The printed name of Hall2 is "East Cell Entrance".
The printed name of Connecting Room is "Cell block Atrium".
The printed name of The Hub is "Bridge Room".
The printed name of BridgeBlock is "Bridge Cell Block Connector End".
The printed name of TeleporterRoom1 is "Bridge Cell Block Connector".
The printed name of TeleporterRoom2 is "Cell Block Bridge Connector".
The printed name of A3 is "Ship Chaplain".
The printed name of A5  is "Cell Block Bridge Connector End".
The printed name of B1 is "Midship".
The printed name of TeleporterRoom3 is "Cell Transfer".
The printed name of B2 is "Hallway".
The printed name of TeleporterRoom4 is "Teleporter Lab".
The printed name of B3 is "Washroom".
The printed name of B4 is "Ship Galley".
The printed name of B5 is "Engine Control Room".
The printed name of Exit is "External Transit Hub".

[Simple things]
A wall is a kind of thing. A wall is undescribed. A wall is fixed in place.
understand "wall" as walls.
[x walls should work for all of these.]
Hall1Wall is a wall. Hall1Wall is in Hall1.
Instead of examining the Hall1Wall:
	say "Upon closer inspection, you see the following image on the wall.".;
	display the figure of spiderman.;

Hall2Wall is a wall. Hall2Wall is in Hall2.
Instead of examining the Hall2Wall:
	say "You see this image on the wall of the room. You know you have seen the face before, but you cannot quite put your finger on where.".;
	display the figure of gggreg.;

CrWall is a wall. CrWall is in Connecting Room.
Instead of examining the CrWall:
	say "You see the following image on the walls.".;
	display the figure of gotnotime.;

brWall is a wall in BridgeBlock.
Instead of examining the brWall:
	say "You see the following image on the walls.".;
	display the figure of hourofhammer.;

telWall is a wall in TeleporterRoom2.
Instead of examining  telWall:
	say "You see this image on the walls.".;
	display the figure of astheyappear.;

chaWall is a wall in A3.
Instead of examining the chaWall:
	say "Just one of the many images on the walls.".;
	display the figure of gaben.;

probWall is a wall in B1.
Instead of examining the probWall:
	say "You see this image on the walls.".;
		display the figure of beer.;
		
labWall is a wall in TeleporterRoom4.
Instead of examining the labWall:
	say "You see this image on the walls.".;
			display the figure of problems.;					

wonWall is a wall in TeleporterRoom1.
Instead of examining the wonWall:
	say "You see this image on the walls.".;
			display the figure of wonka.;


[Things]
A teleporter is a kind of thing.
A teleporter is fixed in place.

A toilet is a kind of thing.
A toilet is fixed in place.

A teleporter1 is a teleporter.
A teleporter2 is a teleporter.
A teleporter3 is a teleporter.
A teleporter4 is a teleporter.

teleporter1 is in TeleporterRoom1.
teleporter2 is in TeleporterRoom2.
teleporter3 is in TeleporterRoom3.
teleporter4 is in TeleporterRoom4.

The printed name of teleporter1 is “toilet”.
The printed name of teleporter2 is “toilet”.
The printed name of teleporter3 is “toilet”.
The printed name of teleporter4 is “west toilet”.

flushing is an action applying to one visible thing.
teleporting is an action applying to one visible thing.

understand the command "ls" as "look".
understand the command "pwd" as "look".
understand the command "cd" as "go".
understand the command "cat" as "examine".

A toilet1 is a toilet.
A toilet2 is a toilet.
A toilet3 is a toilet.
A toilet4 is a toilet.
A toilet5 is a toilet.
A toilet6 is a toilet.
A toilet7 is a toilet.

A toilet1 is in the cell.
A toilet2 is in B3.
A toilet3 is in Exit.
A toilet4 is in BridgeBlock.
A toilet5 is in A5.
A toilet6 is in Hall1.
A toilet7 is in TeleporterRoom4.

The printed name of toilet7 is “east toilet”.
The printed name of toilet6 is “toilet”.
The printed name of toilet5 is “toilet”.
The printed name of toilet4 is “toilet”.
The printed name of toilet3 is “toilet”.
The printed name of toilet2 is “toilet”.
The printed name of toilet1 is “toilet”.

[ THESE NEED TO BE COPIED TO ALL THE TELEPORTER/TOILET OBJECTS IF WE WANT THEM TO BE EVERYWHERE]
Understand "west toilet" as teleporter4.
Understand "east toilet" as toilet7.
Understand "toilet" as toilet or teleporter.

Description of the toilet is "Upon closer inspection, the toilet appears to be a normal toilet."
Description of the teleporter is "Upon closer inspection, the toilet appears to be a normal toilet."

Instead of taking toilet, say "You try, but the toilet is firmly attached to the wall.".
Instead of taking teleporter, say "You try, but the toilet is firmly attached to the wall.".

Instead of going nowhere:
	let rand be a random number between 1 and 100.;
	if rand is greater than 97:
		end the story saying "You bump your head on the walls.[line break][line break]Should have opted for the larger health bar...".;
	otherwise:
		say "You bump your head on the walls. Your life goes down 5 arbitrary points".;

[simple = good]
Rule for printing a parser error when the latest parser error is the I beg your pardon error:
	say "” instead. [Like a terminal!]

Rule for printing a parser error when the latest parser error is the didn't understand error or the latest parser error is the not a verb I recognise error or the latest parser error is the said too little error:
	say "Report on Translation: Failed[line break]
Problem. In the line '<insert irrelevant line>'. I cant find a verb I know how to deal with.[line break]
See the manual: 3.3>3.4 Making all the Errors[line break]

Because of this problem, the source could not be translated into a working game. (Correct the source text to remove the difficulty and click on Go once again.)
	” instead. [ayyy]

Understand "flush [toilet]" as flushing.
Understand "flush me down [toilet]" as flushing.
Understand "flush self down [toilet]" as flushing.
Understand "use [toilet]" as flushing.

Understand "flush me down [teleporter]" as teleporting.
Understand "flush self down [teleporter]" as teleporting.
Understand "use [teleporter]" as teleporting.
Understand "flush [teleporter]" as teleporting.
understand the command "exec" as "flush".

Carry out teleporting:
	if (the noun is in the TeleporterRoom1):
		say "The toilet flushes and you see a green light flash. Everything goes dark. [line break]";
		move player to BridgeBlock;
	else if (the noun is in the TeleporterRoom2):
		say "The toilet flushes and you see a green light flash. Everything goes dark. [line break]";
		move player to A5;
	else if (the noun is in the TeleporterRoom3):
		say "The toilet flushes and you see a green light flash. Everything goes dark. [line break]";
		move player to Hall1;
	else if (the noun is in the TeleporterRoom4):
		say "The toilet flushes and you see a green light flash. Everything goes dark. [line break]";
		move player to TeleporterRoom4;

[hi there =D]
reading is an action applying to nothing.
Understand "I read the source code" as reading.
Carry out reading:
	say "<3".;
	now all fdoor are open.;

Check flushing:
	if (the noun is a toilet1 in the cell):
		[Do one of these for every special toilet]
		say "As you flush the toilet, you feel a sinking feeling in your gut as the air quickly drains out of the room. The wind picks up and sends you barreling through the toilet, and you are ejected into... space? You get a glimpse of a large spaceship as you begin to suffocate. A beam of light illuminates you, and pulls you back through a door into The Cell. The door closes behind you, leaving no traces that it existed.[line break][line break]";
		move player to The Cell instead;
	else if (the noun is not a toilet):
		say "You can't flush that!" instead;
	else if (the noun is the player):
		[This bit seems to never run, 'flush me' is auto-corrected]
		say "You try to find a handle on your body, but no luck" instead;

Carry out flushing:
	if (the noun is a toilet3 in the exit) OR (the noun is the player in the Exit):
		end the story saying "The toilet flushes and you see a green light flash. Everything goes dark. You wake up in your bed at home. You feel disoriented. Your thoughts are jumbled and you take a moment to drink in your surroundings. This is your dorm room back at college. Nothing appears to be out of place. You look at your clock on your bedside table. The time is 6:30 am. Your first thought to yourself is 'what just happened?' You begin to run through your memory of the alien ship, but you cannot help but wonder, was any of it real or was it just a dream. It seems too real to have been a dream.  You can perfectly picture each room as well as the sinking feeling you felt when you flushed the toilets and you got teleported to a new room. Whether or not it was real, you won’t ever forget the feeling of being swept out into space, the instantaneous cold of deep space as well as the suffocating feeling before you were brought back into the ship. It doesn’t matter if it was real, nobody will believe you anyways. All you know for certain is that there is a lot of counselling and therapy in your near future. You won’t ever be able to look at toilets the same way. You know that you should go back to sleep, but you can’t for fear that it was all a dream and that you will be assailed again by the horrible maze filled with strange internet memes. The memes are your biggest clue that it must have been a dream. After all, what sort of alien race would know of human internet memes and worship them so much to cover their ships in them and make them the way to unlock the doors of the ships? It must have been a dream you decide. You decide you might as well go grab breakfast because you won’t get any more sleep tonight. While you are on your way to get breakfast, somewhere down the hall in your dorm, a toilet flushes, the briefest sound of a person’s startled yelp can be heard before it is quickly cut off. Under the cover of clouds in the early morning sky, a spaceship, very similar to the starship enterprise, stealths and steals away back into the cold reaches of space.".;
	else if (the noun is a toilet2):
		say "You try flushing the toilet. As expected, it flushes, however, there are no lights or sounds. As far as you can tell, this is in fact a normal toilet.";
	else:
		say "You flush the toilet, and hear a loud beep and see a red light flash.";


[PUZZLES]

[HUB PUZZLE]
In the Hub is the speaker. The speaker is fixed in place.
The description of the speaker is "There is a small speaker.".

In The Hub is lever.  lever is fixed in place.
The description of the lever is “The meter has a lever beside it and is numbered on the side. Currently, the dial is set to 0, but there are markings in increments of 1000, going all the way up to 9000. Strangely, there is a marking just about 9000 marked '9001'”.
spinning it to is an action applying to one thing and one number.
Check spinning it to:
	if the noun is not the lever, say “[The noun] cannot be set.” instead.;
Report spinning it to:
	say “Nothing much happens. Probably the wrong setting.”.;


Understand “set [lever] to [a number]” as spinning it to.
Understand “pull [lever] to [a number]” as spinning it to.

After spinning the lever to 9001:
	say “Once you push the lever to 9001, a sound begins to play on the speaker and something seems to whir to life. Doors begin to open towards the  west and east, and northeast.”.;
	now the description of The Hub is "A large room, seemingly a central hub.[line break][line break]There are exits south, east, west, and northeast.".;
	now hdoor1 is open.;
	now hdoor2 is open.;
	now hdoor3 is open.;
	play the sound of 9001.;

Understand "gauge" as lever.
Understand "meter" as lever.
[END HUB PUZZLE]

[ Telling people riddles]
A microphone is a kind of animal.
A microphone is either riddlewaiting or inactive.
A microphone is usually inactive.
A microphone is fixed in place.

Understand "mic" as microphone.

mic1 is a microphone.
mic1 is in BridgeBlock.

mic2 is a microphone.
mic2 is in TeleporterRoom3.

mic3 is a microphone.
mic3 is in B4.

mic4 is a microphone.
mic4 is in B5.

mic5 is a microphone.
mic5 is in B3.

mic6 is a microphone.
mic6 is in B1.

The printed name of mic1 is "microphone".
The printed name of mic2 is "microphone".
The printed name of mic3 is "microphone".
The printed name of mic4 is "microphone".
The printed name of mic5 is "microphone".
The printed name of mic6 is "microphone".
Understand "microphone" as microphone.

Instead of telling mic1 about anything:
	try examining mic1.;
Instead of examining mic1:
	say “Out of the microphone, you hear a soft voice: ‘I am a disease. I cause lots of coughing and nobody has time for me, What am I?’”;
	now mic1 is riddlewaiting.
Understand “Bronchitis”, “bronchitis” as banswering.  banswering is an action applying to nothing.
Instead of banswering:
	If the location is BridgeBlock:
		if mic1 is riddlewaiting:
			say “The microphone says, ‘Correct!’, and a door opens to the north.”;
			now bdoor is open.;
			now tdoor1 is open.;
			now mic1 is inactive.;
		otherwise:
			say “What?”;
	otherwise:
		say “Are you talking to someone? =)”


Instead of telling mic2 about anything:
	try examining mic2.;
Instead of examining mic2:
	say “Out of the microphone, you hear a soft voice: ‘He jokes about alien memes… gets abducted that night. He is bad luck _____.’”;
		now mic2 is riddlewaiting.
Understand “Brian”, “brian”, "bad luck brian",
"Bad Luck Brian" as ranswering.  ranswering is an action applying to nothing.
Instead of ranswering:
	If the location is TeleporterRoom3:
		if mic2 is riddlewaiting:
			say “The microphone says, ‘Correct!’, and a door opens to the east.”;
			now gdoor1 is open.;
			now mic2 is inactive.;
		otherwise:
			say “What?”;
	otherwise:
		say “Are you talking to someone? =)”


Instead of telling mic3 about anything:
	try examining mic3.;
Instead of examining mic3:
	say “Out of the microphone, you hear a soft voice: [line break]‘What I if told you[line break][line break]  you read the top line wrong? Write it correctly.’”;
		now mic3 is riddlewaiting.
Understand “What if I told you”, “what if I told you”, “what if i told you”, "Morpheus", "morpheus" as manswering.
manswering is an action applying to nothing.
Instead of manswering:
	If the location is B4:
		if mic3 is riddlewaiting:
			say “The microphone says, ‘Correct!’, and a door opens to the north.”;
			now gdoor2 is open.;
			now mic3 is inactive.;
		otherwise:
			say “What?”;
	otherwise:
		say “Are you talking to someone? =)”


Instead of telling mic4 about anything:
	try examining mic4.;
Instead of examining mic4:
	say “Out of the microphone, you hear a soft voice: ‘I got abducted by aliens, now how do I check facebook? ____ world problems’”;
		now mic4 is riddlewaiting.
Understand “First”, “first” as fanswering.
fanswering is an action applying to nothing.
Instead of fanswering:
	If the location is B5:
		if mic4 is riddlewaiting:
			say “The microphone says, ‘Correct!’, and a door opens to the north.”;
			now edoor1 is open.;
			now mic4 is inactive.;
		otherwise:
			say “What?”;
	otherwise:
		say “Are you talking to someone? =)”

Instead of telling mic5 about anything:
	try examining mic5.;
Instead of examining mic5:
	say “Out of the microphone, you hear a soft voice: ‘Got abducted by aliens huh? Who can help you flush your way out of this one. ’”;
		now mic5 is riddlewaiting.
Understand “Wonka”, “wonka”, "Willy Wonka", "willy", "Willy", "willy wonka", "condescending wonka", "Condescending Wonka", "guy in purple suit" as wanswering.
wanswering is an action applying to nothing.
Instead of wanswering:
	If the location is B3:
		if mic5 is riddlewaiting:
			say “The microphone says, ‘Correct!’, and a door opens to the east.”;
			now edoor2 is open.;
			now mic5 is inactive.;
		otherwise:
			say “What?”;
	otherwise:
		say “Are you talking to someone? =)”
		
Instead of telling mic6 about anything:
	try examining mic6.;
Instead of examining mic6:
	say “Out of the microphone, you hear a soft voice: ‘He doesnt always escape spaceships, but when he does it's through the toilet. He is the most ______ man in the world.’”;
			now mic6 is riddlewaiting.
Understand “interesting”, “Interesting” as ianswering.
ianswering is an action applying to nothing.
Instead of ianswering:
	If the location is B1:
		if mic6 is riddlewaiting:
			say “The microphone says, ‘Correct!’, and a door opens to the north.”;
			now bdoor1 is open.;
			now mic6 is inactive.;
		otherwise:
			say “What?”;
	otherwise:
		say “Are you talking to someone? =)”

[ Special Release]
Release along with the source text.
Release along with an interpreter.
Release along with cover art.

[Testing]
Test me with "ne/ne/flush/sw/set lever to 9001/w/ne/flush/x microphone/bronchitis/n/n/w/ne/n/x wall/x mic/interesting/n/e/x microphone/brian/e/n/x microphone/what if I told you/n/n/x microphone/first/n/s/s/w/n/n/e/x microphone/wonka/e/".



